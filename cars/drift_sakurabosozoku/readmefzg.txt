FZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZG
fzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzg
FZGfzgFZGfzgFZGfzgFZGfzg# f r 13 n d z 0 n 3 d g u y #FZGfzgFZGfzgFZGfzgFZG
fzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzg
FZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZG

Car information
================================================================
Car name        	: Sakura Bosozoku

Car Type  		: remodel

Top speed		: 38 mph

Rating/Class   		: 3 (semi-pro)

Installed folder       	: ...\cars\sakurabosozoku

Description           	: This car is the remodeled version of
			  Sakura by KIPY
			  

Author Information
================================================================
Author Name 	: F13ndz0n3dguy

Email Address   : fatahzahran@ymail.com or gmail.com


Construction
================================================================
Base           	: Sakura by KIPY

Editor(s) used 	: Blender and Paint.net


Additional Credits 
================================================================
Thanks to the whole community who continues to make Re-Volt a fun and attractive game !
HUKI with the amazing engine SFX (and other optimization) updates
Mightycucumber and R6TE who taught me blender basics
all y'all members of the community responses on discord
All the updates of RVGL


Copyright / Permissions
================================================================
You may do whatever you want with this car but you have to give us credits and give credit to the original author. 
Do not distribute without authorization.

