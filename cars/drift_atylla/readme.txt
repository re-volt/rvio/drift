Car information
================================================================
Car name : Attyla
Car Type : Original
Top speed : 40 mph
Rating/Class : 4 (pro)
Installed folder : ...\cars\Attyla
Description : This is a car which was originally made for a competition (finally canceled I think)
: 
: 

Author Information
================================================================
Author Name : Halogaland
Email Address : halogaland@orange.fr
Other Info : 

Construction
================================================================
Base : Adeon
Editor(s) used : MSPaint, and Zmodeler 1.07b

Additional Credits 
================================================================


Copyright / Permissions
================================================================
Do what you want with this car (repaint, conversions for other games, etc...) but you MUST give me credits

