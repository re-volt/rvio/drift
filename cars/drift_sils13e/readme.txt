Car information
================================================================
Car name                : Nissan silvia s13 DRIFT
Car Type  		: Repaint 
Top speed 		: 49 mph/kph
Rating/Class   		: ameteur
Install folder       	: ...\cars\nissild
Description             : This car is my attempt at creating a drift car for revolt
			im guessing some may not like how it handles but once 
			you get used to it, it is really fun. There is an extra paintjob if you dont
			want the crazy coloured one
tips			: pick a fast flat track like toys in the hoodor a double sized 
			lego track. keep your foot to the floor (or fingers to the keyboard)
			try not to opposite steer too much, if your going too fast turn into 
			the corner more, too slow then press the brake(sounds crazy i know)
			have fun!!!!

 
Author Information
================================================================
Author Name 		: MOH
Email Address           : carmadman@hotmail.com	
Misc. Author Info       : 
 
Construction
================================================================
Base           		: nissan 240sx (themeandme)
Poly Count     		: ? polies for the body
               		: ? polies for each wheel
Editor(s) used 		:  photoshop 7, ms paint
 
Additional Credits 
================================================================
themeandme for their racekit that i used for the wing.
 
Copyright / Permissions
================================================================
Authors MAY use this Car as a base to build additional cars.  
You MAY distribute this CAR, provided you include this file, with no 
modifications.  You may distribute this file in any electronic format 
(BBS, Diskette, CD, etc) as long as you include this file intact.

 
Where else to get this CAR
================================================================
FTP sites		:
Website  		:
Other			:
