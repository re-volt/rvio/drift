================================================================
Car Information
================================================================
Car Name  : Nissan 240SX S14
Car Type  : Conversion
Folder	  : ...\cars\nissil14
Install   : Unzip with folder names on to the main ReVolt folder
Top speed : 46 mph
Rating    : Pro

================================================================
Author Information
================================================================
Author Name : The Me and Me
EMail       : saver@gmx.li
Homepage    : http://www.themeandme.de/

================================================================
Car Description
================================================================
Some call it Silvia, some call it S14, some call it 240SX, some
call it 200SX, some call it Kouki... One things for shizzle,
it's a Nissan. And it's pretty flippin nice, better than the S15
thats what it is. So here you go have some fun. We tried to do a
chameleon like paintjob looks quite good. Params are capable of
quite a lot, beaten our ReadMe time for El Dorado with them.
Have fun!

================================================================
Construction
================================================================
Base           : NFS4 custom model by Ryuji Kainoh
Poly Count     : 759 polies for the body
               : 150 polies for each wheel
Editor(s) used : PSP 7; ZModeler; RVShade; RVSizer; RVTexmap 
Known Bugs     : None

================================================================
Thanks And Accolades
================================================================
We want to thank you, for downloading this car, and supporting
the Re-Volt community with dlding it. Then we gotta thank the
whole RV community, too. Its probably the best community on a
racing game all around the web, and it's hard work to get there.
Now we all have to work even harder to keep it there! Thanks
and: Keep it up!

We want to thank Wayne LeMonds, for his great site and forum. If
it Wayne would not have been here, you would possibly not read
this, cuz we never would have gone so deep into Re-Volt without
the forum. Thanks a whole bunch, Wayne! If you havn't visited
his website please do so, you won't regret it.
http://www.revoltdownloads.com/

We want to thank the whole staff of RVA for their great site. It
is the perfect addition to Racerspoint and will be (or probably
is already) the completest Re-Volt site on the web, with all the
cars, tracks, tutorial, tools etc. If you don't know it yet, you
really have to hurry to get there: http://www.rvarchive.com/
or http://www.sportplanet.com/rva/

We would also like to thank ALL the peeps at #revolt-chat on
the IRC austnet servers for their support and help without
forgetting anyone of them. Thanks dudes!

Then we want to thank all the creators of the tools we used.
Without their hard work, this car would not have been possible.

================================================================
Individual Thanks On This Car
================================================================
incogbla:
For Touge Run 1, the real deal for this kinda car. Pretty hard
to get a decent shot on tho, but the result is good. Thanks!

scloink:
Damn, this is again a car that uses scloink's wheels. They look
great even if they have been used for a thousand times. Dunno
what to say, blabla. Thanks!

Ryuji Kainoh:
For this great looking model, which is very realistic and cool.
We were lost without him producing all these cars, and we're
lucky that he does 'em. Thanks!

================================================================
Copyright / Permissions
================================================================
Authors MAY use this Car as a base to build additional cars,
provided you give proper credit to the creators of the parts
from this car that were used to built the new car. E.g. if
you use the wheel models give credit to the creators and/or
converters. 
 
You MAY distribute this CAR, provided you include this file,
with no modifications.  You may distribute this file in any
electronic format (BBS, Diskette, CD, etc) as long as you
include this file intact.

================================================================
Where to get this CAR
================================================================
Websites : http://www.revoltdownloads.com/
         : http://www.sportplanet.com/rva/
	 : http://www.revoltunlimited.co.uk/
	 : http://www.themeandme.de/

