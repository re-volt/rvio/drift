Nissan 240SX S14 for Need for speed IV

Title          : Nissan 240SX S14
Car            : Nissan 240SX[S14] (SN:40)
File           : ns14.zip
Version        : 1.0 (Upgrade Feature : NO)
Date           : JAN 2001

Author         : Ryuji KAINOH
Email          : ryuji_k@pop13.odn.ne.jp
Homepage       : http://www1.odn.ne.jp/ryuji_k/nfs3.htm

Used Editor(s) : Mrc(cartool.zip) by EA
               : NFS Wizard v0.5.0.79 by Jesper Juul-Mortensen
               : VIV Wizard v0.8(build 297) by Jesper Juul-Mortensen
               : VIV Wizard v0.8(build 299) by Jesper Juul-Mortensen
               : NFSIII Car CAD v1.4b by Chris Barnard
               : NFS Car CAD v1.5b by Chris Barnard
               : CAR3TO4 by J�rg Billeter
               : NFS FCEConverter by Addict&Rocket
               : PaintShop Pro 5J
Thanks.
___________________________________________________________

Installation : Put the "car.viv" in "Data\Cars\ns14".
               and  the "ns14.qfs" in "Data\FeArt\VidWall".

Have a fun !!


...Formula1(1987to2000) Carset for ICR2(IndycarRacing2)...
http://www1.odn.ne.jp/ryuji_k/