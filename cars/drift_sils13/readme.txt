Car information
================================================================
Car name                : Silvia S13 Drift
Car Type  		: Repaint 
Top speed 		: 49 mph/kph
Rating/Class   		: pro
Install folder       	: ...\cars\s13
Description             : S13 Drift car, stable and not twitchy, and can ,maintain drifts longer
				but, the drift needs to be started earlier to get a nice one!
 
Author Information
================================================================
Author Name 		: MOH
Email Address           : carmadman@hotmail.com	
Misc. Author Info       : 
 
Construction
================================================================
Base           		: original
Poly Count     		: ? polies for the body
               		: ? polies for each wheel
Editor(s) used 		:  photoshop 7,zmodeler,rvshade,rvsizer,rvdblsd,rvtrans
 
Additional Credits 
================================================================
Everyone whos still plays revolt for keeping the game alive
everyone on rvzt for keeping each other into revolt.
 
Copyright / Permissions
================================================================
Authors MAY use this Car as a base to build additional cars.  
You MAY distribute this CAR, provided you include this file, with no 
modifications.  You may distribute this file in any electronic format 
(BBS, Diskette, CD, etc) as long as you include this file intact.

 
Where else to get this CAR
================================================================
FTP sites		:
Website  		:
Other			:
