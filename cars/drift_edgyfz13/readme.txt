FZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZG
FZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZG
FZGfzgFZGfzgFZGfzgFZGfzg# f r 13 n d z 0 n 3 d g u y #FZGfzgFZGfzgFZGfzgFZG
FZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZG
FZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZGfzgFZG
__________________________________________________________________________

Car information
__________________________________________________________________________

Car name        	: Drift Edgy FZ-13

Car Type  		: repaint/reparam

Top speed Drift Pro	: 35-41mph / 15-19mph (drifting)

Rating/Class   		: 4 (pro)

Installed folder       	: ...\drift_edgyfz13

Description           	: This car is a remodel of halogaland's boost 
			  extreme car


Author Information
__________________________________________________________________________

Author Name 	: Fr13ndz0n3dguy

Email Address   : fatahzahran@ymail.com or gmail.com

Other Info	: ReVolt user since 2001 !


Construction
__________________________________________________________________________

Base           	: Boost from HALOGALAND'S extreme car pack

Editor(s) used 	: Blender and Paint.net


Additional Credits 
__________________________________________________________________________

-Thanks to the whole community who continues to make Re-Volt a fun and 
attractive game !
-HALOGALAND for the Boost car from his extreme car pack
-All y'all members resposes on Re-Volt Discord server
-R6TE and Mightycucumber who taught me blender basics
-All the updates of RVGL


Copyright / Permissions
__________________________________________________________________________

You may do whatever you want with this car but you have to give us 
credits and give credit to the original author.
Do not distribute without authorization.
 
