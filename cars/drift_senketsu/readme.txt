Car information
================================================================
Car name             	: Senketsu Drift

Car Type  		: Original

Top speed 		: 32.5mph (theoretical) / 53kph (observed)

Rating/Class   		: 0 (rookie)

Installed folder       	: ...\cars\senketsu_d

Description           	: I wanted another onroad car : here it is.
                        : The params are for drifting, thus I recommend using this car on a flat track.
			: Two other versions are available.

 

Author Information
================================================================
Author Name 		: Z3R0L33T

Email Address		: z3r0l33t@hotmail.fr

Other Info		: ReVolt user since 1999 !



Construction
================================================================
Base           		: Original concept car

Editor(s) used 		: Blender, Paint.net, Photoshop CS6


 
Additional Credits 
================================================================
Thanks to the whole community who continues to make ReVolt a fun and attractive game !



Copyright / Permissions
================================================================
You may do whatever you want with this car but you have to give me credits. Do not distribute without authorization.
 
