{

;============================================================
;============================================================
; Senketsu
;============================================================
;============================================================
Name      	"D1GP Senketsu"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
;)CPUSelectable	FALSE
;)Statistics 	TRUE
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	-200 			; Skill level (rookie, amateur, ...)
TopEnd     	2968.014648 			; Actual top speed (mph) for frontend bars
Acc        	3.887978 			; Acceleration rating (empirical)
Weight     	2.600000 			; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 			; Max Revs (for rev counter, deprecated...)

;====================
; Model Filenames
;====================

MODEL 	0 	"cars\drift_senketsu\body.prm"
MODEL 	1 	"cars\drift_senketsu\wheel_l.prm"
MODEL 	2 	"cars\drift_senketsu\wheel_r.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
COLL 	"cars\drift_senketsu\hull.hul"
TPAGE 	"cars\drift_senketsu\car.bmp"
;)TSHADOW 	"NONE"
;)SHADOWINDEX 	-1
;)SHADOWTABLE 	0.000000 0.000000 0.000000 0.000000 0.000000 	; Left, right, front, back, height (relative to model center)
;)TCARBOX   	"cars\drift_senketsu\carbox.bmp" 		; Carbox texture
EnvRGB 	200 200 200

;====================
; Handling related stuff
;====================

SteerRate  	2.500000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	32.500000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 0.000000 0.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon generation offset
;)Flippable	FALSE 			; Rotor car effect
;)Flying   	FALSE 			; Flying like the UFO car
;)ClothFx  	FALSE 			; Mystery car cloth effect

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 0.000000 0.000000
Mass       	2.000000
Inertia    	1800.000000 0.000000 0.000000
           	0.000000 3500.000000 0.000000
           	0.000000 0.000000 750.000000
Gravity    	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air resistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	30.000000 			; AngRes scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-26.000000 -4.750000 38.000000
Offset2  	-2.500000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.350000
EngineRatio 	32500.000000
Radius      	9.900000
Mass        	0.150000
Gravity     	4400.000000
MaxPos      	7.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	-7.500000
AxleFriction    	0.020000
Grip            	0.002000
StaticFriction  	0.700000
KineticFriction 	0.700000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	26.000000 -4.750000 38.000000
Offset2  	2.500000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.350000
EngineRatio 	32500.000000
Radius      	9.900000
Mass        	0.150000
Gravity     	4400.000000
MaxPos      	7.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	-7.500000
AxleFriction    	0.020000
Grip            	0.002000
StaticFriction  	0.700000
KineticFriction 	0.700000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-26.000000 -4.750000 -29.000000
Offset2  	-2.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	32500.000000
Radius      	9.900000
Mass        	0.150000
Gravity     	4400.000000
MaxPos      	7.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	-7.500000
AxleFriction    	0.020000
Grip            	0.001800
StaticFriction  	0.700000
KineticFriction 	0.700000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	26.000000 -4.750000 -29.000000
Offset2  	2.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	32500.000000
Radius      	9.900000
Mass        	0.150000
Gravity     	4400.000000
MaxPos      	7.000000
SkidWidth   	10.000000
ToeIn       	0.000000
;)Camber    	-7.500000
AxleFriction    	0.020000
Grip            	0.001800
StaticFriction  	0.700000
KineticFriction 	0.700000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
;)Type      	1 			; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 3.000000 6.000000 		; Translation max
;)TransVel  	0.001000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	-12.000000 -27.000000 -26.000000
Direction   	0.000000 -1.000000 0.000000
Length      	17.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	172.690002
UnderRange  	3223.403076
UnderFront  	221.550003
UnderRear   	335.000000
UnderMax    	0.858176
OverThresh  	814.809326
OverRange   	1391.000000
OverMax     	0.949048
OverAccThresh  	415.170959
OverAccRange   	363.401611
PickupBias     	3276
BlockBias      	6553
OvertakeBias   	22936
Suspension     	0
Aggression     	0
}           	; End AI

}

