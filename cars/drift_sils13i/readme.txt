Originally modeled and textured by MOH, i took it upon myself to alter the parameters a bit
to make it more competetive with other popular drift cars, and spruced up the texture while
i was at it. I've tried to keep the original handling mostly intact. As such, you can
expect smooth and reliable drifts with only a very mild tendency to oversteer in a drift.

The original car can be found here:
http://revoltzone.net/cars/16288/Nissan%20Silvia%20S13%20Tuned