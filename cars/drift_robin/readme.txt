﻿03:45 21/02/2012

	~ ~ ~ ~ ~ ~ ~ ~ ~ ~ SKARMINATER　プレゼント ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

			 <Tweaked by Citywalker>

		     	     <Reliant Robin>
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Preface)

The slowest car I've ever made. If you can win a race with this, you deserve a medal.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Car Information)

Date	  : 20/12/2011
Car Name  : Reliant Robin
Car Type  : Conversion
Folder    : ...\cars\robin
Install   : As with any custom car, just drag the cars folder into your Re-Volt folder.
Top Speed : 25 MPH (observed)
Rating    : Barely even Rookie class...
Car #     : 313
Release # : 38

A few were demanding that there MUST be a Robin in Re-Volt. So here it is! Made more realistic by CW, just watch your steering.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Construction)

Base           : NFS4 model by Bob Robert.
Editor(s) used : NFS Wizard, Zanoza Modeler 1.07a, Car:Load, PRM2HULL, Adobe Photoshop, Paint, Notepad
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Thanks)

Bob Robert for the excellent model of this classic british motor.
Citywalker for her fantastic tinkering. Seriously, where would we be without her skills? ;)
MOH for the HMS Invincible track where I took the picture.
hilaire9 for his massive selection of tracks.
Zach/ZAgames for hosting RVZT and working on the next generation RVZ.
Zipperrulez for individually helping me with various things, Minecraft, teaching me how to colour and various other crap.
DSL_Tile for keeping #ReVolt-Chat going.
Nero for NIGGRANADA!
Mezza - My main man! @_@
Kajeuter for being a FAT-BERTHA!
KDL for the Car:Load and CarLighter tools. They are what keep me in buisness!
Jigebren for his PRM2HULL tool.
Everyone else in RVHouse for some great fun races.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
(Copyrights)

I can gurantee you'll want to race this more than modify it but you know the drill on my cars...

If your username is Tua, sobar2 or Redencion or ANY of the ridiculous amount of other aliases you use, you are not allowed to edit or reupload this car in any way, shape, or form whatsoever.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
(Where to find this car)

This car was uploaded to RVZT. It may also be found on other sites such as RVZ, RVA, ARM in due time...

Visit my WIP thread at RVL to see what other cars I have going on! Link underneath.

＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿

http://z3.invisionfree.com/Revolt_Live/index.php?showtopic=1595
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Disclaimer)

By downloading this car, you are using it at your own risk & discretion. I am not responsible for ANY problems that this car may cause to your computer.

Please, no complaints about rail handling, high poly, etc...
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
Have fun! Thank you and good night! Much love!