Information
-----------------
Car Name:		BP Apex Kraft Trueno
Top speed:	39 mph
Rating:		Semi-Pro
Author:		Nero


Description
-----------------
A JGTC GT300 car from the late 90's. Also includes drifting parameters.


Credits
-----------------
Polyphony Digital for the car and textures
Oleg for ZModeler
Ali for RVMinis
Huki and Jigebren for RV 1.2
Jigebren for PRM2HUL
Rick Brewster for Paint.NET
Citywalker for the car AI tutorial
Everyone in #ReVolt-Chat for the amusing conversations


Permission
-----------------
You are free to do anything with the car for personal use, but if you submit a repaint or a remodel of this car or convert this car to another game, give credit to Polyphony Digital and myself.