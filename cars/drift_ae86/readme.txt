Car information
================================================================
Car name                : AE86 Drift
Car Type  		: original
Top speed 		: 37 mph/kph
Rating/Class   		: pro
Install folder       	: ...\cars\AE86D
Description             : JDM AE86 Drift car

 
Author Information
================================================================
Author Name 		: MOH 
Email Address           : carmadman@hotmail.com	
Misc. Author Info       : 
 
Construction
================================================================
Base           		: original
Poly Count     		: ? polies for the body
               		: ? polies for each wheel
Editor(s) used 		:  GIMP,zmodeler,rvshade,rvsizer,rvdblsd,rvtrans
 
Additional Credits 
================================================================
Everyone whos still plays revolt for keeping the game alive
everyone on rvzt for keeping each other into revolt.
RLS for the amazing paintjob
 
Copyright / Permissions
================================================================
Authors MAY use this Car as a base to build additional cars.  
You MAY distribute this CAR, provided you include this file, with no 
modifications.  You may distribute this file in any electronic format 
(BBS, Diskette, CD, etc) as long as you include this file intact.

 
Where else to get this CAR
================================================================
FTP sites		:
Website  		:
Other			:
