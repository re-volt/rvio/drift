Car information
================================================================
Car name                :Sileighty drift
Car Type  		: Repaint 
Top speed 		: 49 mph/kph
Rating/Class   		: pro
Install folder       	: ...\cars\nissil80
Description             :Nissan 180 sx, with a silvia S13 front, drifter! it feels light and slippy, and is easier to drift
			but harder to maintain a long one
 
Author Information
================================================================
Author Name 		: MOH
Email Address           : carmadman@hotmail.com	
Misc. Author Info       : 
 
Construction
================================================================
Base           		: original
Poly Count     		: ? polies for the body
               		: ? polies for each wheel
Editor(s) used 		:  photoshop 7,zmodeler,rvshade,rvsizer,rvdblsd,rvtrans
 
Additional Credits 
================================================================
Everyone whos still plays revolt for keeping the game alive
everyone on rvzt for keeping each other into revolt.
 
Copyright / Permissions
================================================================
Authors MAY use this Car as a base to build additional cars.  
You MAY distribute this CAR, provided you include this file, with no 
modifications.  You may distribute this file in any electronic format 
(BBS, Diskette, CD, etc) as long as you include this file intact.

 
Where else to get this CAR
================================================================
FTP sites		:
Website  		:
Other			:
