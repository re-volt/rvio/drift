XRT conversion from Live for Speed to Re-Volt
  Converted by Balesz
  Parameters by Kipy
===================

Some info about this car:
  As it's the middle brother in the XR family, it's not the slowest
   and not the fastest car, which make it pretty balanced for racing.
  It's RWD as it should be.

===================

I hope you'll like this conversion too!
There are some extra skins in the folder.
If you have a custom XRT skin, you can make it ingame by putting the .png in the folder as an overlay and remove 0;0;0 (like to 0;0;2)
===================

Sources and used programs:
Photoshop
Blender
LFS Ready-To-Render kit by Bogey Jammer: https://goo.gl/PbtQLn (link to forum)